public class UE
{
    private String intitule;
    private String codeUE;
    private int inscrits;
    private  int MaxEtudiant;
    private Etudiant[] tab_inscrits;
    private double[] tab_notes;

    public UE(String intitule, String codeUE, int nbMax)
    {
        this.intitule=intitule;
        this.codeUE=codeUE;
        MaxEtudiant=nbMax;
        tab_inscrits=new Etudiant[MaxEtudiant];
        tab_notes= new double[MaxEtudiant] ;
        inscrits=0;
    }
    public boolean inscrire_etudiant(Etudiant e)
    {      
        if(indice(e.getNumero())!=-1)
            return false;
        if(this.UEPlein())
        {
            return false;
        }
        tab_inscrits[inscrits]=e;
        inscrits++;
        e.ajouterUE(this);
        return true;
    }
    public String getIntitule()
    {
        return intitule;
    }
    public String getCodeUE()
    {
        return codeUE;
    }
    public String toString()
    {   
        return this.intitule+" "+this.codeUE+" nombre inscrits: "+this.inscrits;
    }
    public boolean UEPlein()
    {
        return inscrits==MaxEtudiant;
    }
    private int indice(int numeroEtd)
    {
        int i=0;
        for(i=0;i<inscrits;i++)
        {
            if(tab_inscrits[i].getNumero()==numeroEtd)
                return i;
        }
        
        return -1;
    }
    public void set_notes(int numero_etu, double note)
    {
        if(this.indice(numero_etu)==-1)
        {
            System.out.println("Erreur!");
            return;
        }
        tab_notes[indice(numero_etu)]=note;
    }
    public double moyenneUE()
    {
        double moy=0;
        for(int i=0;i<inscrits;i++)
        {
            moy+=tab_notes[i];           
        }
        return moy/inscrits;
    }
}