import java.util.Scanner;
import java.io.PrintWriter;

public class TestUniversite
{
    public static void main(String[] args) throws java.io.IOException
    {
        Etudiant ali=new Etudiant("ali", 143);
        Etudiant bine=new Etudiant("bine", 123);
        Etudiant ami=new Etudiant("ami", 183);

        UE maths= new UE("Maths", "123", 15);
        UE sport= new UE("Sport", "321", 15);

        

        Universite Galilee=new Universite(30,30);
        Galilee.ajouterUE(maths);
        Galilee.ajouterUE(sport);
        Galilee.inscrire_etudiant(ali);
        
        Galilee.inscrire_etudiant(ami);

        Galilee.inscrire_dans_UE(143, "123");
        Galilee.inscrire_dans_UE(183, "321");
        Galilee.inscrire_dans_UE(183, "123");

        maths.set_notes(143, 15);
        Galilee.attribuer_note(183, "123", 15);
        Galilee.attribuer_note(183, "321", 16);
        
        Galilee.afficherNoteUEs();
        
        // Base de Données
        PrintWriter writer1 = new PrintWriter("ING_INFO.txt");
        writer1.println(Galilee);
        writer1.println(ali);
        writer1.println(bine);
        writer1.println(ami);
        writer1.close();


        
    }
}