import java.util.Scanner;
import java.io.PrintWriter;
import java.io.File;

public class Universite
{
    private int NB_max_etudiants;
    private int NB_max_UE;
    private UE[] tab_UEs;
    private int nombreUEs;
    private Etudiant[] tab_etudiants;
    private int nombreEtudiants;
    public Universite(int maxetu, int maxue)
    {
        this.NB_max_etudiants=maxetu;
        this.NB_max_UE= maxue;
        this.tab_UEs=new UE[NB_max_UE];
        this.tab_etudiants=new Etudiant[NB_max_etudiants];
        this.nombreUEs=0;
        this.nombreEtudiants=0;

    }
    /*
    public Universite(File fichier)
    {
        String nomFichier= fichier.getName();
        java.io.File fic2 = new java.io.File(nomFichier);
        Scanner  lecteur2= new Scanner(fic2);

        String  []temp;

    }
    */
    public void afficherUEs()
    {
        int i=0;
        for(i=0; i<this.nombreUEs; i++)
        {
           System.out.println(tab_UEs[i]);
        }

    }
    public void afficherEtudiants()
    {
        int i=0;
        for(i=0; i<this.nombreEtudiants; i++)
        {
           System.out.println(tab_etudiants[i]);
        }   
    }
    public int indice_etudiants(int numero)
    {
        int i=0;
        for(i=0;i<this.nombreEtudiants;i++)
        {
            if(tab_etudiants[i].getNumero()==numero)
                return i;
        }
        
        return -1;       
    }
    public int indice_UE(String codeUE)
    {
        int i=0;
        for(i=0;i<this.nombreEtudiants;i++)
        {
            if(tab_UEs[i].getCodeUE()==codeUE)
                return i;
        }
        
        return -1;       
    }
    public boolean inscrire_etudiant(Etudiant e)
    {
        if(this.indice_etudiants(e.getNumero())!=-1)
            return false;

        tab_etudiants[nombreEtudiants]=e;
        this.nombreEtudiants++;
        return true;   
    }
    public boolean ajouterUE(UE ue)
    {
        if(this.indice_UE(ue.getCodeUE())!=-1)
            return false;

        tab_UEs[nombreUEs]=ue;
        this.nombreUEs++;
        return true;        
    }
    public void inscrire_dans_UE(int numero_etu, String codeUE)
    {
        int a=indice_UE(codeUE);
        int b=indice_etudiants(numero_etu);
        
        if(a!=-1)
        {   
            
            if(b==-1)
            {
                System.out.println("Inscription administrative d'abord");
                return;
            }
            tab_UEs[a].inscrire_etudiant(tab_etudiants[b]);
            

        }    
    }
    public void attribuer_note(int numero_etu, String codeUE, double note)
    {
        if(indice_UE(codeUE)!=-1)
        {
            tab_UEs[indice_UE(codeUE)].set_notes(numero_etu, note);
        }
       
    }
    public void afficherNoteUEs()
    {
        for(int i=0;i<nombreUEs;i++)
        {
            System.out.print(tab_UEs[i]);
            System.out.println(" Moyenne="+tab_UEs[i].moyenneUE());
        }
    }
    public String toString()
    {
        return "Université: Nombre max d'étudiants:"+this.NB_max_etudiants+"; Nombre max d'UEs"+this.NB_max_UE;
    }
}