public class Etudiant
{
    private String nom;
    private int numero;
    private int nombreUE;
    private UE[] TabUE;
    private int cmpt;
    static final int NB_gabarit=10;

    public Etudiant( String nom, int numero)
    {
        this.nom=nom;
        this.numero=numero;
        TabUE= new UE[NB_gabarit];
        nombreUE=0;
        cmpt=0;
    }
    public boolean ajouterUE(UE e)
    {
        if(this.EtudiantComplet())
        {
            return false;
        }
        TabUE[cmpt]=e;
        cmpt++;
        return true;
    }
    public String toString()
    {   
        int i=0;
        String s="";
        for(i=0; i<cmpt; i++)
        {
            s+=TabUE[i]+"; ";
        }
        return this.nom+" "+this.numero+" Inscrits dans les UEs: "+s;
    }
    public boolean EtudiantComplet()
    {
        return cmpt==NB_gabarit;
    }
    public int getNumero()
    {
        return numero;
    }
    public String getNom()
    {
        return nom;
    }


}